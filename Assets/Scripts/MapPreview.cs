﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPreview : MonoBehaviour
{
    public Renderer textureRenderer;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    // Determines what the map generator displays
    public enum DrawMode { NoiseMap, Mesh, FalloffMap }
    public DrawMode drawMode;

    public MeshSettings meshSettings;
    public HeightMapSettings heightMapSettings;
    public TextureSettings textureSettings;

    [Range(0, MeshSettings.NUM_SUPPORTED_LEVELS_OF_DETAIL - 1)]
    public int editorPreviewLevelOfDetail;

    public Material terrainMaterial;

    public bool autoUpdate;

    public void DrawMapInEditor()
    {
        // Initialize the terrain material
        textureSettings.ApplyToMaterial(terrainMaterial);

        // Send the min and max heights of this terrain to the texture data struct so that it is able to be used in the terrain shader
        textureSettings.UpdateMeshHeights(terrainMaterial, heightMapSettings.minHeight, heightMapSettings.maxHeight);

        Texture2D mapTexture;

        // Generate the HeightMap
        HeightMap heightMap = HeightMapGenerator.GenerateHeightMap(meshSettings.numVertsPerLine, meshSettings.numVertsPerLine, heightMapSettings, Vector2.zero);

        // Get a texture to display for the selected draw mode
        switch (drawMode)
        {
            case DrawMode.NoiseMap:

                // Grab the texture from the raw height map
                mapTexture = TextureGenerator.GenerateTextureFromHeightMap(heightMap);

                // Draw the texture
                DrawTexture(mapTexture);

                break;

            case DrawMode.Mesh:

                // Generate the 3D terrain mesh
                MeshData meshData = MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, editorPreviewLevelOfDetail);

                // Draw the generated mesh
                DrawMesh(meshData);

                break;

            case DrawMode.FalloffMap:

                // Generate the height map for the falloff map
                HeightMap falloffHeightMap = new HeightMap(FalloffGenerator.GenerateFalloffMap(meshSettings.numVertsPerLine), 0, 1);

                // Use the generated falloffHeightMap to generate a texture
                mapTexture = TextureGenerator.GenerateTextureFromHeightMap(falloffHeightMap);

                // Draw the texture
                DrawTexture(mapTexture);

                break;

            default:
                Debug.LogError("Invalid draw type " + drawMode + " not supported");
                return;
        }
    }

    public void DrawTexture(Texture2D texture)
    {
        // Apply the texture to the material, and scale the plane to match the width and height of the passed in texture
        textureRenderer.sharedMaterial.mainTexture = texture;
        textureRenderer.transform.localScale = new Vector3(texture.width, 1, texture.height) / 10f;

        textureRenderer.gameObject.SetActive(true);
        meshFilter.gameObject.SetActive(false);
    }

    public void DrawMesh(MeshData meshData)
    {
        meshFilter.sharedMesh = meshData.CreateMesh();

        textureRenderer.gameObject.SetActive(false);
        meshFilter.gameObject.SetActive(true);
    }

    /// <summary>
    /// Sets the OnValuesUpdated action of the updateable data to be the map generator's OnValuesUpdated method.
    /// This makes it so that when the UpdateableData hits its OnValidate method, it calls the MapPreview callback method supplied here.
    /// This is used to have the map generator re-draw the generated map with new data from the UpdateableData classes when they are updated
    /// </summary>
    /// <param name="updateableData"></param>
    private void SubscribeToUpdateableDataEvent(UpdateableData updateableData, System.Action action)
    {
        // To prevent the queing of many calls to the OnValuesUpdated callback method, clear out the previously requested callback method.
        // An alternative to this would be to loop through the updateableData.GetInvocationList (from within the UpdateableData class) and check to see if we've already subscribed with the MapPreview's OnValuesUpdated method
        updateableData.OnValuesUpdated -= action;

        // Set the OnValuesUpdated callback action to be the MapPreview's OnValuesUpdated method
        updateableData.OnValuesUpdated += action;
    }

    /// <summary>
    /// This gets run any time the values in the inspector have been updated.
    /// </summary>
    private void OnValidate()
    {
        if (meshSettings != null)
        {
            SubscribeToUpdateableDataEvent(meshSettings, OnValuesUpdated);
        }
        if (heightMapSettings != null)
        {
            SubscribeToUpdateableDataEvent(heightMapSettings, OnValuesUpdated);
        }
        if (textureSettings != null)
        {
            SubscribeToUpdateableDataEvent(textureSettings, OnTextureValuesUpdated);
        }
    }

    void OnValuesUpdated()
    {
        if (!Application.isPlaying)
        {
            DrawMapInEditor();
        }
    }

    void OnTextureValuesUpdated()
    {
        textureSettings.ApplyToMaterial(terrainMaterial);
    }
}
