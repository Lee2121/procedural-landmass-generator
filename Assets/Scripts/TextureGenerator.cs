﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureGenerator
{
    public static Texture2D GenerateTextureFromColorMap(Color[] colorMap, int width, int height)
    {
        Texture2D texture = new Texture2D(width, height);
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.SetPixels(colorMap);
        texture.Apply();
        return texture;
    }

    public static Texture2D GenerateTextureFromHeightMap(HeightMap heightMap)
    {
        int width = heightMap.values.GetLength(0);
        int height = heightMap.values.GetLength(1);

        Texture2D texture = new Texture2D(width, height);

        Color[] colorMap = new Color[width * height];

        // Using the passed in height map, fill out the color map array with colors between black and white
        for (int currHeight = 0; currHeight < height; currHeight++)
        {
            for (int currWidth = 0; currWidth < width; currWidth++)
            {
                // Normalize the height map value so that it is within the range of our lowest and highest points
                float normalizedHeightMapValue = Mathf.InverseLerp(heightMap.minValue, heightMap.maxValue, heightMap.values[currWidth, currHeight]);

                // Use the normalized height map value to determine the color at this coord
                colorMap[currHeight * width + currWidth] = Color.Lerp(Color.black, Color.white, normalizedHeightMapValue);
            }
        }

        // Generate a texture from the black and white color map
        return GenerateTextureFromColorMap(colorMap, width, height);
    }
}
