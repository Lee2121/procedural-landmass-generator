﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HeightMapGenerator
{
    public static HeightMap GenerateHeightMap(int width, int height, HeightMapSettings settings, Vector2 sampleCenter)
    {
        float[,] values = PerlinNoiseMap.GenerateNoiseMap(width, height, settings.noiseSettings, sampleCenter);

        // Needed because of issues with sharing this height curve across multiple threads.
        // Each thread needs to have its own instance of the height curve, so create a duplicate of it here
        AnimationCurve heightCurve_threadsafe = new AnimationCurve(settings.heightCurve.keys);

        float minValue = float.MaxValue;
        float maxValue = float.MinValue;

        for(int currWidth = 0; currWidth < width; currWidth++)
        {
            for(int currHeight = 0; currHeight < height; currHeight++)
            {
                values[currWidth, currHeight] *= heightCurve_threadsafe.Evaluate(values[currWidth, currHeight]) * settings.heightMultiplier;

                if(values[currWidth, currHeight] > maxValue)
                {
                    maxValue = values[currWidth, currHeight];
                }
                if(values[currWidth, currHeight] < minValue)
                {
                    minValue = values[currWidth, currHeight];
                }
            }
        }

        return new HeightMap(values, minValue, maxValue);
    }
}

[System.Serializable]
public struct HeightMap
{
    public readonly float[,] values;
    public readonly float minValue;
    public readonly float maxValue;

    public HeightMap(float[,] values, float minValue, float maxValue)
    {
        this.values = values;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }
}
