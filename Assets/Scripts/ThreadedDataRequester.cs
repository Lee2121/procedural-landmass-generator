﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

public class ThreadedDataRequester : MonoBehaviour
{
    private static ThreadedDataRequester instance;
    private Queue<ThreadInfo> dataQueue = new Queue<ThreadInfo>();

    private void Awake()
    {
        instance = FindObjectOfType<ThreadedDataRequester>();
    }

    /// <summary>
    /// Starts up a new thread which, when finished, calls the callback function
    /// </summary>
    /// <param name="callback">The function to send the computed HeightMap struct back to once calculations have completed</param>
    public static void RequestData(Func<object> generateData, Action<object> callback)
    {
        ThreadStart threadStart = delegate { instance.DataThread(generateData, callback); };
        new Thread(threadStart).Start();
    }

    /// <summary>
    /// Handles the thread for generating the requested data
    /// </summary>
    /// <param name="callback">Once the calculations have completed, the map data struct will be passed back into this function</param>
    private void DataThread(Func<object> generateData, Action<object> callback)
    {
        object data = generateData();
        // Lock the thread info queue so that only the current thread is able to access it. Without this, it's possible that two threads could attempt to access the same data at the same time, which would be bad.
        lock (dataQueue)
        {
            dataQueue.Enqueue(new ThreadInfo(callback, data));
        }
    }

    private void Update()
    {
        // Look through our map data thread info queue, and call the requested callback functions to send the map data struct back to the requester
        if (dataQueue.Count > 0)
        {
            for (int iMapThreadInfo = 0; iMapThreadInfo < dataQueue.Count; iMapThreadInfo++)
            {
                ThreadInfo threadInfo = dataQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    // Stores generated map info (such as map or mesh data), along with a call back function to send that generated data to.
    // Used to thread map data generation so that map generation can be properly load balanced
    public struct ThreadInfo
    {
        public readonly Action<object> callback;
        public readonly object parameter;

        public ThreadInfo(Action<object> callback, object parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }
}
