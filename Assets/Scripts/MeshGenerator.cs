﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator
{
    #region Vertex Type Getters

    enum VertexType
    {
        Invalid,
        Skipped,        // Vertex that isn't drawn into the mesh due to level of detail simplification
        OutOfMesh,      // Vertex that is only used to calculate normals, and won't be used as part of the mesh
        EdgeConnection, // Vertex that is used to create detailed geometry on the edges of the mesh, which is used to fix seams along meshes with different levels of detail
        MeshEdge,       // Vertex that is on the outermost edge of the mesh
        Main            // Vertex that is in the center of the mesh, that is not skipped due to the level of detail simplification
    }

    /// <summary>
    /// Determines if the specified vertex is outside the mesh.
    /// This is done by checking to see if the vertex is within the first couple border vertices, which are not used when drawing the mesh, and are only used to calculate normals.
    /// </summary>
    private static bool IsVertexOutOfMeshVertex(int vertexX, int vertexY, int numVertsPerLine)
    {
        return (vertexY == 0 || vertexY == numVertsPerLine - 1 || vertexX == 0 || vertexX == numVertsPerLine - 1);
    }

    /// <summary>
    /// Determines if the specified vertex is on the outermost edge of the mesh
    /// </summary>
    private static bool IsVertexMeshEdgeVertex(int vertexX, int vertexY, int numVertsPerLine)
    {
        // A vertex cannot be both an out of mesh vertex and a mesh border vertex
        if (IsVertexOutOfMeshVertex(vertexX, vertexY, numVertsPerLine))
        {
            return false;
        }

        return (vertexX == 1 || vertexX == numVertsPerLine - 2 || vertexY == 1 || vertexY == numVertsPerLine - 2);
    }

    /// <summary>
    /// Determines if the specified vertex is part of the simplified mesh. Exludes the edge vertices, as they'll be returned in IsVertexMeshEdgeVertex
    /// </summary>
    private static bool IsVertexMainVertex(int vertexX, int vertexY, int numVertsPerLine, int skipIncrement)
    {
        return (vertexX - 2) % skipIncrement == 0 && (vertexY - 2) % skipIncrement == 0;
    }

    /// <summary>
    /// Determines if the specifed vertex is an edge connection, meaning that is is one of the mesh vertices that is used to link different LOD meshes
    /// </summary>
    private static bool IsVertexEdgeConnectionVertex(int vertexX, int vertexY, int numVertsPerLine, int skipIncrement)
    {
        return vertexX == 2 || vertexX == numVertsPerLine - 3 || vertexY == 2 || vertexY == numVertsPerLine - 3;
    }

    /// <summary>
    /// Determines if the specified vertex is a "skipped vertex", or a vertex that will not be drawn into the mesh due to level of detail simplification
    /// </summary>
    private static bool IsVertexSkippedVertex(int vertexX, int vertexY, int numVertsPerLine, int skipIncrement)
    {
        // If the vertex is on the border of the mesh, it cannot be skipped
        if (vertexX <= 2 || vertexX >= numVertsPerLine - 3 || vertexY <= 2 || vertexY >= numVertsPerLine - 3)
        {
            return false;
        }

        // If the vertex is a "main vertex", or one that still needs to be used despite the level of detail simplification, it cannot be a skipped vertex
        if (IsVertexMainVertex(vertexX, vertexY, numVertsPerLine, skipIncrement))
        {
            return false;
        }

        // The supplied vertex can be skipped due to the mesh simplification
        return true;
    }

    /// <summary>
    /// Gets the type of vertex found given the vertex data.
    /// </summary>
    private static VertexType GetVertexType(int vertexX, int vertexY, int numVertsPerLine, int skipIncrement)
    {
        if(IsVertexSkippedVertex(vertexX, vertexY, numVertsPerLine, skipIncrement))
        {
            return VertexType.Skipped;
        }
        
        if(IsVertexOutOfMeshVertex(vertexX, vertexY, numVertsPerLine))
        {
            return VertexType.OutOfMesh;
        }

        if(IsVertexMeshEdgeVertex(vertexX, vertexY, numVertsPerLine))
        {
            return VertexType.MeshEdge;
        }

        if(IsVertexMainVertex(vertexX, vertexY, numVertsPerLine, skipIncrement))
        {
            return VertexType.Main;
        }

        if(IsVertexEdgeConnectionVertex(vertexX, vertexY, numVertsPerLine, skipIncrement))
        {
            return VertexType.EdgeConnection;
        }

        // If we got down here, something went wrong. The vertex should be one of the above
        Debug.LogError("Was unable to find a vertex type for the current vertex: vertexX " + vertexX + " - vertexY " + vertexY + " - numVertsPerLine " + numVertsPerLine + " - skipIncrement " + skipIncrement);
        return VertexType.Invalid;
    }

    private static bool ShouldTriangleBeCreatedForVertex(int vertexX, int vertexY, int numVertsPerLine, VertexType vertexType)
    {
        // Triangles are created clockwise from the top left vertex. If this vertex is on the bottom most or right most edge of the mesh, it should not create a triangle
        if(vertexX >= numVertsPerLine - 1 || vertexY >= numVertsPerLine - 1)
        {
            return false;
        }

        // If this is an edge connection, we don't want the vertex to create a triangle if the vertex is in the second row or column.
        // This is because the geometry found here should only be created by the main vertices, due to the created triangle being on the interior of the mesh and not on the border
        if(vertexType == VertexType.EdgeConnection && (vertexX == 2 || vertexY == 2))
        {
            return false;
        }

        // The vertex should create a triangle
        return true;
    }

    /// <summary>
    /// Calculates the height for the given edge connection vertices.
    /// Since the edge connection vertices aren't always found next to a main vertex, there can sometimes be gaps in the mesh due to the height difference of the edge connection vertex and the nearby mesh.
    /// To address this, we get the height of the nearby main vertices, and use a weighted average towards the closer one to grab a new height which ensures there are no seams.
    /// </summary>
    private static float GetHeightForEdgeConnectionVertex(int vertexX, int vertexY, float[,] heightMap, int numVertsPerLine, int skipIncrement)
    {
        // Determines whether or not to use the x or y axis to look for the surrounding main vertices
        bool isVertical = vertexX == 2 || vertexX == numVertsPerLine - 3;

        // Grab the distance to the first nearest vertex
        int distToMainVertexA = ((isVertical) ? vertexY - 2 : vertexX - 2) % skipIncrement;
        
        // Use the first nearest vertex to get our distance to the next one
        int distToMainVertexB = skipIncrement - distToMainVertexA;

        float distPercentFromAToB = distToMainVertexA / (float)skipIncrement;

        // Get the heights of main vertex A and B
        float heightMainVertexA = heightMap[(isVertical) ? vertexX : vertexX - distToMainVertexA, (isVertical) ? vertexY - distToMainVertexA : vertexY];
        float heightMainVertexB = heightMap[(isVertical) ? vertexX : vertexX + distToMainVertexB, (isVertical) ? vertexY + distToMainVertexB : vertexY];

        // Return the weighted height based on how close the vertex is to the nearby main vertices A and B
        return heightMainVertexA * (1 - distPercentFromAToB) + heightMainVertexB * distPercentFromAToB;
    }

    #endregion

    /// <summary>
    /// Generates a 2D int array which holds the index for a given vertex.
    /// Verticies that are on the edge of the bounds are border vertices, and are used solely to calculate normals to avoid lighting seams.
    /// Verticies that are within the border are mesh vertices, and are the vertices that are actually displayed when drawing the mesh
    /// </summary>
    /// <param name="numVertsPerLine">The width of the mesh INCLUDING the border vertices. Assumed to be a square</param>
    /// <param name="skipIncrement">The level of detail simplification value. Simplifies the mesh by skipping over unneeded vertices</param>
    /// <returns></returns>
    private static int[,] GenerateVertexIndicesMap(int numVertsPerLine, int skipIncrement)
    {
        int[,] vertexIndicesMap = new int[numVertsPerLine, numVertsPerLine];
        int meshVertexIndex = 0;
        int outOfMeshVertexIndex = -1;

        for (int currHeight = 0; currHeight < numVertsPerLine; currHeight++)
        {
            for (int currWidth = 0; currWidth < numVertsPerLine; currWidth++)
            {
                bool isOutOfMeshVertex = IsVertexOutOfMeshVertex(currWidth, currHeight, numVertsPerLine);
                bool isSkippedVertex = IsVertexSkippedVertex(currWidth, currHeight, numVertsPerLine, skipIncrement);

                if (isOutOfMeshVertex)
                {
                    vertexIndicesMap[currWidth, currHeight] = outOfMeshVertexIndex;
                    outOfMeshVertexIndex--;
                }
                else if(!isSkippedVertex)
                {
                    vertexIndicesMap[currWidth, currHeight] = meshVertexIndex;
                    meshVertexIndex++;
                }
            }
        }

        return vertexIndicesMap;
    }

    public static MeshData GenerateTerrainMesh(float[,] heightMap, MeshSettings meshSettings, int levelOfDetail)
    {
        // Used to control the LOD of the map mesh. Modifies how many vertices are actually created and positioned when creating the terrain mesh
        // We need to make sure that the mesh simplification increment is at least 1, otherwise our for loops through the width and height will never finish.
        int skipIncrement;
        if (levelOfDetail == 0)
        {
            skipIncrement = 1;
        }
        else
        {
            skipIncrement = levelOfDetail * 2;
        }

        int numVertsPerLine = meshSettings.numVertsPerLine;

        // Used to center the vertex points
        Vector2 topLeft = new Vector2(-1, 1) * meshSettings.meshWorldSize / 2f;

        MeshData meshData = new MeshData(numVertsPerLine, skipIncrement, meshSettings.useFlatShading);

        // Generate the map which tells us which indices are border vertices and which ones are mesh indices
        int[,] vertexIndicesMap = new int[numVertsPerLine, numVertsPerLine];
        vertexIndicesMap = GenerateVertexIndicesMap(numVertsPerLine, skipIncrement);

        for (int currHeight = 0; currHeight < numVertsPerLine; currHeight++)
        {
            for (int currWidth = 0; currWidth < numVertsPerLine; currWidth++)
            {
                // Get the type of vertex this is
                VertexType vertexType = GetVertexType(currWidth, currHeight, numVertsPerLine, skipIncrement);

                // Don't run the below logic on skipped vertices
                if(vertexType == VertexType.Skipped)
                {
                    continue;
                }                

                // Grab our index from the mapping
                int vertexIndex = vertexIndicesMap[currWidth, currHeight];

                // Create a new uv for the vertext which holds the percentage of the x and y axis that this vertex is found at. Need to subtract 1 from the width and height and 3 from the num verts to account for the border vertices. We only want to consider the actual mesh vertices.
                Vector2 percent = new Vector2(currWidth - 1, currHeight - 1) / (numVertsPerLine - 3);

                // Calculate the height of the vertex by using the animation curve to force the vertex to a certain height, and then apply the height multiplier to it
                float vertexHeight = heightMap[currWidth, currHeight];

                // If this is an edge connection vertex, we need to set its height to be an average between the nearest two main vertices
                if(vertexType == VertexType.EdgeConnection)
                {
                    vertexHeight = GetHeightForEdgeConnectionVertex(currWidth, currHeight, heightMap, numVertsPerLine, skipIncrement);                    
                }

                // Compute the position for the vertex. -percent.y is used because triangles are created from the top down
                Vector2 vertexPosition2D = topLeft + new Vector2(percent.x, -percent.y) * meshSettings.meshWorldSize;
                Vector3 vertexPosition = new Vector3(vertexPosition2D.x, vertexHeight, vertexPosition2D.y);

                // Add the vertex to the mesh data
                meshData.AddVertex(vertexPosition, percent, vertexIndex);

                // Creates 2 triangles, which together form a square
                if(ShouldTriangleBeCreatedForVertex(currWidth, currHeight, numVertsPerLine, vertexType))
                {
                    // Get the size to create the triangle at. Main vertices will create triangles based on the skipIncrement, while the border vertices will create smaller unit triangles
                    // If this is a main vertex that is not on the bottom or right most edge of the mesh, use the level of detail determined skip increment. 
                    // Otherwise, we need to make a small triangle with a size of 1 
                    int currentIncrement = (vertexType == VertexType.Main && currWidth != numVertsPerLine - 3 && currHeight != numVertsPerLine - 3) ? skipIncrement : 1;
                    
                    // Get the indicies for all the vertices in the two triangles
                    int a = vertexIndicesMap[currWidth, currHeight];
                    int b = vertexIndicesMap[currWidth + currentIncrement, currHeight];
                    int c = vertexIndicesMap[currWidth, currHeight + currentIncrement];
                    int d = vertexIndicesMap[currWidth + currentIncrement, currHeight + currentIncrement];

                    // Add the two triangles to the mesh
                    meshData.AddTriangle(a,d,c);
                    meshData.AddTriangle(d,a,b);
                }
            }
        }

        // Compute the desired shading of the generated mesh
        meshData.ProcessMeshShading();

        return meshData;
    }
}

public class MeshData
{
    private Vector3[] vertices;
    private int[] triangles;
    private Vector2[] uvs;
    private Vector3[] bakedNormals;

    private Vector3[] outOfMeshVertices;
    private int[] outOfMeshTriangles;

    private int triangleIndex;
    private int outOfMeshTriangleIndex;

    private bool useFlatShading;

    public MeshData(int numVertsPerLine, int skipIncrement, bool useFlatShading)
    {
        int numMeshEdgeVertices = (numVertsPerLine - 2) * 4 - 4; // numVertsPerLine - 2 to get rid of border vertices, times 4 to account for each side, - 4 so we don't double count the corners
        int numEdgeConnectionVertices = (skipIncrement - 1) * (numVertsPerLine - 5) / skipIncrement * 4; // We need to skip each of the main vertices, which the edge connection vertices share lines with. skipIncrement - 1 to account for this, times the numVertsPerLine - 5 to only count the mesh vertices in each line divided by the skip count to get the number of groups of edge connection vertices, times 4 to get each side
        int numMainVerticesPerLine = (numVertsPerLine - 5) / skipIncrement + 1; // numVertsPerLine - 5 to only count vertices that are in the mesh, divided by the skip count + 1 to get the number of times a main vertex appears including the last one
        int numMainVertices = numMainVerticesPerLine * numMainVerticesPerLine; // Squared numMainVerticesPerLine gives us the total number of main vertices

        vertices = new Vector3[numMeshEdgeVertices + numEdgeConnectionVertices + numMainVertices];
        uvs = new Vector2[vertices.Length]; // same size as the vertices array

        int numMeshEdgeTriangles = 8 * (numVertsPerLine - 4); // ((numVertsPerLine - 3) * 4 - 4) * 2; // numVertsPerLine - 3 to get rid of the border triangles (-2) and because the num of tris is 1 less than the num of verts (-1), then times 4 for each side, minus for to account for corners, times 2 because each square has 2 triangles
        int numMainTriangles = (numMainVerticesPerLine - 1) * (numMainVerticesPerLine - 1) * 2; // numMainVerticesPerLine - 1 because num of tris is 1 less than num of verts, squared to get number of squares, times 2 because each square has 2 triangles
        triangles = new int[(numMeshEdgeTriangles + numMainTriangles) * 3]; // total number of triangles, times 3 to get the number of vertices (since the triangles array is actually an array of ordered vertices

        outOfMeshVertices = new Vector3[numVertsPerLine * 4 - 4]; // Number of vertices in the base mesh times 4 for the border sides, minus 4 because otherwise we'll be double counting at each corner
        outOfMeshTriangles = new int[((numVertsPerLine - 1) * 4 - 4) * 2 * 3]; // number of vertices - 1 to get num of squares along one side of the mesh, times 4 to account for each side, - 4 so that we don't double count the corners, times 2 because each square contains 2 triangles, times 3 because each triangle has 3 vertices

        this.useFlatShading = useFlatShading;
    }

    public void AddVertex(Vector3 vertexPosition, Vector2 uv, int vertexIndex)
    {
        // Determine if the vertex is part of the border, or if it is part of the mesh
        if(vertexIndex < 0)
        {
            // For border vertices, the vertex index will start at -1 and go down from there. To index into the border vertices array, make the passed in vertex index positive and subtract 1 so that we start at index 0
            outOfMeshVertices[-vertexIndex - 1] = vertexPosition;
        }
        else
        {
            vertices[vertexIndex] = vertexPosition;
            uvs[vertexIndex] = uv;
        }
    }

    /// <summary>
    /// Adds a triangle defined by vertices a, b, and c to the triangles array.
    /// </summary>
    public void AddTriangle(int a, int b, int c)
    {
        // Determine if the triangle is part of the border, or if it is part of the mesh
        if(a < 0 || b < 0 || c < 0)
        {
            outOfMeshTriangles[outOfMeshTriangleIndex] = a;
            outOfMeshTriangles[outOfMeshTriangleIndex + 1] = b;
            outOfMeshTriangles[outOfMeshTriangleIndex + 2] = c;

            outOfMeshTriangleIndex += 3;
        }
        else
        {
            triangles[triangleIndex] = a;
            triangles[triangleIndex + 1] = b;
            triangles[triangleIndex + 2] = c;

            triangleIndex += 3;
        }
    }

    Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC)
    {
        // Grab the proper vertex depending on if the vertex index is on the border or in the mesh. We negate and subtract 1 from the borderVertices array as the index will be < 0 if it is a border index
        Vector3 pointA = (indexA < 0) ? outOfMeshVertices[-indexA - 1] : vertices[indexA];
        Vector3 pointB = (indexB < 0) ? outOfMeshVertices[-indexB - 1] : vertices[indexB];
        Vector3 pointC = (indexC < 0) ? outOfMeshVertices[-indexC - 1] : vertices[indexC];

        Vector3 sideAB = pointB - pointA;
        Vector3 sideAC = pointC - pointA;
        return Vector3.Cross(sideAB, sideAC).normalized;
    }

    Vector3[] CalculateNormals()
    {
        Vector3[] vertexNormals = new Vector3[vertices.Length];

        // The triangles array stores sets of three vertices, so divide the length by 3 to get the number of unique triangles
        int triangleCount = triangles.Length / 3;
        int outOfMeshTriangleCount = outOfMeshTriangles.Length / 3;

        // Loop through all the mesh triangles and calculate the normals
        for(int currTriangle = 0; currTriangle < triangleCount; currTriangle++)
        {
            // Grab the indices for each of the current triangle's vertices
            int normalTriangleIndex = currTriangle * 3;
            int vertexIndexA = triangles[normalTriangleIndex];
            int vertexIndexB = triangles[normalTriangleIndex + 1];
            int vertexIndexC = triangles[normalTriangleIndex + 2];

            // Get the surface normal for the triangle, and set each vertex to have that normal
            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
            vertexNormals[vertexIndexA] += triangleNormal;
            vertexNormals[vertexIndexB] += triangleNormal;
            vertexNormals[vertexIndexC] += triangleNormal;
        }

        // Loop through all the border triangles and calculate normals
        for (int currOutOfMeshTriangle = 0; currOutOfMeshTriangle < outOfMeshTriangleCount; currOutOfMeshTriangle++)
        {
            // Grab the indices for each of the current triangle's vertices
            int normalTriangleIndex = currOutOfMeshTriangle * 3;
            int vertexIndexA = outOfMeshTriangles[normalTriangleIndex];
            int vertexIndexB = outOfMeshTriangles[normalTriangleIndex + 1];
            int vertexIndexC = outOfMeshTriangles[normalTriangleIndex + 2];

            // Get the surface normal for the triangle, and set each vertex to have that normal
            Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);

            // Only save off the vertex normal of the index if it is going to be part of our mesh. The other vertex normals will have a vertex index of < 0, and won't be displayed
            if (vertexIndexA >= 0)
            {
                vertexNormals[vertexIndexA] += triangleNormal;
            }
            if (vertexIndexB >= 0)
            {
                vertexNormals[vertexIndexB] += triangleNormal;
            }
            if (vertexIndexC >= 0)
            {
                vertexNormals[vertexIndexC] += triangleNormal;
            }
        }

        // Normalize the list of normals
        for (int currVertex = 0; currVertex < vertexNormals.Length; currVertex++)
        {
            vertexNormals[currVertex].Normalize();
        }

        return vertexNormals;
    }

    private void FlatShading()
    {
        Vector3[] flatShadedVertices = new Vector3[triangles.Length];
        Vector2[] flatShadedUvs = new Vector2[triangles.Length];

        // Sets all triangles to have unique vertices. No two triangles should share the same vertices when flat shaded (as well as UVs)
        for (int currTriangle = 0; currTriangle < triangles.Length; currTriangle++)
        {
            // Fill the current triangle with its unique vertices and UVs
            flatShadedVertices[currTriangle] = vertices[triangles[currTriangle]];
            flatShadedUvs[currTriangle] = uvs[triangles[currTriangle]];

            // Store off the newly constructed triangle back into the triangles array
            triangles[currTriangle] = currTriangle;
        }

        // Apply the flat shaded vertices and uvs to the mesh
        vertices = flatShadedVertices;
        uvs = flatShadedUvs;
    }

    private void BakeNormals()
    {
        bakedNormals = CalculateNormals();
    }

    /// <summary>
    /// Once the mesh is generated, this should be called so that the appropriate processing is done to finalize how the newly generated mesh should be shaded.
    /// If the mesh should use flat shading, processing will be done to ensure that each triangle in the mesh has its own unique set of vertices, so that no vertex will be shared by two triangles.
    /// If the mesh should not use flat shading, the normals of the mesh will be baked, which will ensure that there are no seams between unique terrain chunks.
    /// </summary>
    public void ProcessMeshShading()
    {
        if(useFlatShading)
        {
            FlatShading();
        }
        else
        {
            BakeNormals();
        }
    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        
        if (useFlatShading)
        {
            mesh.RecalculateNormals(); // Use the built in logic to calculate the normals of the mesh
        }
        else
        {
            mesh.normals = bakedNormals; // Use the normals that we calculated ourselves through the custom baking process
        }

        return mesh;
    }
}