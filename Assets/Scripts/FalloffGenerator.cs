﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FalloffGenerator
{
    /// <summary>
    /// Generates a 2d map where coordinates near the edge have a value of 1 and coordinates near the center have a value of -1
    /// </summary>
    /// <param name="size">The width of the 2d map</param>
    /// <returns>A 2d float array where coordaintes near the edge have a value of 1 and coordiantes near the center have a value of -1</returns>
    public static float[,] GenerateFalloffMap(int size)
    {
        float[,] map = new float[size, size];

        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                // Derive a value between -1 and 1 at this coordinate by determining how far away the coordinate is from the center of the area
                float x = i / (float)size * 2 - 1;
                float y = j / (float)size * 2 - 1;

                // Take the absolute value of the x and y values and choose whichever is greater
                float value = Mathf.Max(Mathf.Abs(x), Mathf.Abs(y));
                map[i, j] = ApplyFalloffCurve( value );
            }
        }

        return map;
    }

    /// <summary>
    /// Applies a modifying curve to a given falloff map value.
    /// This is used to have more control over the falloff map.
    /// a controls the slope of the falloff map transitioning from culling to drawing.
    /// b controls the offset of the falloff map, making it so the center of the transition area occurrs farther or closer to the edge/center of the map
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    static float ApplyFalloffCurve(float value)
    {
        float a = 3.0f;
        float b = 2.2f;

        return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
    }

    /// <summary>
    /// Applies the falloff map to the height map.
    /// </summary>
    /// <param name="heightMap"></param>
    /// <param name="falloffMap"></param>
    /// <returns>The 2d height map after applying the supplied falloff map to it</returns>
    public static float[,] ApplyFalloffMapToHeightMap( float[,] heightMap, float[,] falloffMap )
    {
        // Make sure that the height map and the falloff map has the same dimensions
        if(heightMap.GetLength(0) != falloffMap.GetLength(0) || heightMap.GetLength(1) != falloffMap.GetLength(1))
        {
            Debug.Assert((heightMap.GetLength(0) != falloffMap.GetLength(0) || heightMap.GetLength(1) != falloffMap.GetLength(1)), "The supplied height map and falloff map do not have the same dimensions");
            return new float[1,1];
        }

        // Loop through the height map and apply the value found in the falloff map
        for(int y = 0; y < heightMap.GetLength(1); y++)
        {
            for(int x = 0; x < heightMap.GetLength(0); x++)
            {
                // Grab the falloff value from the falloff map and apply to the height map. Ensure the heightmap value is still within bounds
                float falloffValue = falloffMap[x, y];
                heightMap[x, y] = Mathf.Clamp01(heightMap[x, y] - falloffValue);
            }
        }

        return heightMap;
    }
}
