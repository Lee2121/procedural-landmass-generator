﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateableData : ScriptableObject
{
    public event System.Action OnValuesUpdated;
    public bool autoUpdate;

#if UNITY_EDITOR

    protected virtual void OnValidate()
    {
        if(autoUpdate)
        {
            // Fixes a race condition where OnValidate for the UpdateableData is called first, then the shader which uses the terrain texture is recompiled afterwards.
            // This causes the terrain mesh to become untextured when scripts are recompiled.
            // This call makes it so that when the editor application updates (after everything has been compiled, etc.), NotifyOfUpdatedValues will be called.
            UnityEditor.EditorApplication.update += NotifyOfUpdatedValues;
        }
    }

    public void NotifyOfUpdatedValues()
    {
        // As soon as we call this, make sure to unsubscribe this function from the editor application update
        UnityEditor.EditorApplication.update -= NotifyOfUpdatedValues;

        if (OnValuesUpdated != null)
        {
            OnValuesUpdated();
        }
    }

#endif

}
