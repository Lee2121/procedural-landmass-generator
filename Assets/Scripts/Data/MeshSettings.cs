﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MeshSettings : UpdateableData
{
    public const int NUM_SUPPORTED_LEVELS_OF_DETAIL = 5;

    public const int NUM_SUPPORTED_CHUNK_SIZES = 9;
    public const int NUM_SUPPORTED_FLATSHADED_CHUNK_SIZES = 3;

    // The supported chunk sizes for each mesh type. These need to be cleanly divisible by each number from 1 to NUM_SUPPORTED_LEVELS_OF_DETAIL - 1.
    public static readonly int[] supportedChunkSizes = { 48, 72, 96, 120, 144, 168, 192, 216, 240 };

    // The x,y, and z scale of the terrain chunk
    public float meshScale = 2f;
    
    // If set, each triangle in the mesh will have its own unique set of vertices. This makes it so that rather than smoothly blending normals between adjacent triangles, the normals will othoganal to their triangle.
    // This produces a "flat" effect, where each triangle is distinctly visible
    public bool useFlatShading;

    [Range(0, NUM_SUPPORTED_LEVELS_OF_DETAIL - 1)]
    public int chunkSizeIndex;
    [Range(0, NUM_SUPPORTED_FLATSHADED_CHUNK_SIZES - 1)]
    public int flatshadedChunkSizeIndex;

    /// <summary>
    /// The number of vertices per line of the mesh rendered at the highest level of detail.
    /// Because of the LOD system, this number needs to be divisible by all of the numbers within MeshGenerator.NUM_SUPPORTED_LEVELS_OF_DETAIL. 
    /// If flat shading is being used, each triangle requires three of its own unique vertices which cannot be shared by any other triangle in the mesh.
    /// Because of this, and the fact that unity only supports meshes with ~65000 vertices, a smaller terrain chunk size must be used if flat shading is requested
    /// </summary>
    public int numVertsPerLine
    {
        get
        {
            // Determine which size index we should use
            int sizeIndex = useFlatShading ? flatshadedChunkSizeIndex : chunkSizeIndex;

            // Plus 5 because there are 4 extra points per mesh.
            // The "out of mesh" vertices are on the outside, and are used to properly generate normals to remove shading seams. +2 (one for the start of each line and one for the end)
            // The next vertices in are the "mesh edge vertices", which are used to draw a high resolution band around the edge of the mesh to ensure that meshes with differend LODs still match up. +2
            // 2 + 2 plus an additional 1 because the supported chunk size starts at 0, but we still need to count the first vertex.
            return supportedChunkSizes[sizeIndex] + 5;
        }
    }

    /// <summary>
    /// How much space the actual mesh takes up.
    /// </summary>
    public float meshWorldSize
    {
        get
        {
            // - 3 because we subtract 1 from the numVertsPerLine to get the actual size of the mesh created by that many vertices (if there are 3 verts, it has a size of 2)
            // We then subtract an additional 2 to make it -3 total because we don't want to include the border vertices that are used solely to compute normals
            return (numVertsPerLine - 3) * meshScale;
        }
    }
}
