﻿using UnityEngine;
using System.Collections;
using System.Linq;

[CreateAssetMenu()]
public class TextureSettings : UpdateableData
{
    const int TEXTURE_SIZE = 512;
    const TextureFormat TEXTURE_FORMAT = TextureFormat.RGB565;

    public Layer[] layers;

    private float savedMinHeight;
    private float savedMaxHeight;
    
    /// <summary>
    /// Initializes the material shader values so that the material is able to display properly
    /// </summary>
    /// <param name="material"></param>
    public void ApplyToMaterial(Material material)
    {
        material.SetInt("layerCount", layers.Length);
        material.SetColorArray("baseColors", layers.Select(x => x.tint).ToArray());
        material.SetFloatArray("baseStartHeights", layers.Select(x => x.startHeight).ToArray());
        material.SetFloatArray("baseBlends", layers.Select(x => x.blendStrength).ToArray());
        material.SetFloatArray("baseColorStrengths", layers.Select(x => x.tintStrength).ToArray());
        material.SetFloatArray("baseTextureScales", layers.Select(x => x.textureScale).ToArray());

        Texture2DArray texturesArray = GenerateTextureArray(layers.Select(x => x.texture).ToArray());
        material.SetTexture("baseTextures", texturesArray);

        UpdateMeshHeights(material, savedMinHeight, savedMaxHeight);
    }

    public void UpdateMeshHeights(Material material, float minHeight, float maxHeight)
    {
        savedMinHeight = minHeight;
        savedMaxHeight = maxHeight;

        material.SetFloat("minHeight", minHeight);
        material.SetFloat("maxHeight", maxHeight);
    }

    Texture2DArray GenerateTextureArray(Texture2D[] textures)
    {
        Texture2DArray textureArray = new Texture2DArray(TEXTURE_SIZE, TEXTURE_SIZE, textures.Length, TEXTURE_FORMAT, true);

        for(int currTexture = 0; currTexture < textures.Length; currTexture++)
        {
            textureArray.SetPixels(textures[currTexture].GetPixels(), currTexture);
        }
        textureArray.Apply();
        return textureArray;
    }

    [System.Serializable]
    public class Layer
    {
        public Texture2D texture;
        public float textureScale;

        public Color tint;

        [Range(0, 1)]
        public float tintStrength;

        [Range(0, 1)]
        public float startHeight;

        [Range(0, 1)]
        public float blendStrength;
    }
}
