﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HeightMapSettings : UpdateableData
{
    public NoiseSettings noiseSettings;

    // If set, a falloff map will be used to fade the edges of the height map to 0. Causes the generated map to resemble an island.
    public bool useFalloffMap;

    // Multiplier which causes the differences in height map values to be more pronounced in the terrain mesh
    public float heightMultiplier;

    // Curve which is used to modify the effects of the heightMultiplier at varying heights on the height map
    public AnimationCurve heightCurve;

    // Minimum possible height of the terrain based on the terrain parameters
    public float minHeight
    {
        get { return heightMultiplier * heightCurve.Evaluate(0); }
    }

    // Maximum possible height of the terrain based on the terrain parameters
    public float maxHeight
    {
        get { return heightMultiplier * heightCurve.Evaluate(1); }
    }

#if UNITY_EDITOR

    protected override void OnValidate()
    {
        noiseSettings.ValidateValues();
        base.OnValidate();
    }

#endif
}