﻿using UnityEngine;

public class TerrainChunk
{
    const float COLLIDER_GENERATION_DISTANCE_THRESHOLD = 5.0f;

    public event System.Action<TerrainChunk, bool> onVisibilityChanged;

    public Vector2 coord;

    // Used for determining viewer proximity
    GameObject meshObject;
    Vector2 sampleCenter;
    Bounds bounds;
    float maxViewDistance;

    // Renders the terrain chunk
    MeshRenderer meshRenderer;
    MeshFilter meshFilter;

    // Collider
    MeshCollider meshCollider;
    int colliderLevelOfDetailIndex;
    bool hasSetCollider;

    // Tracks the detail levels and the associated mesh
    LevelOfDetailInfo[] detailLevels;
    LevelOfDetailMesh[] levelOfDetailMeshes;
    int previousLevelOfDetailIndex = -1;

    HeightMap heightMap;
    bool heightMapReceived;

    HeightMapSettings heightMapSettings;
    MeshSettings meshSettings;

    Transform viewer;

    public TerrainChunk(Vector2 coord, HeightMapSettings heightMapSettings, MeshSettings meshSettings, LevelOfDetailInfo[] detailLevels, int colliderLevelOfDetailIndex, Transform parent, Transform viewer, Material material)
    {
        this.coord = coord;

        this.detailLevels = detailLevels;
        this.colliderLevelOfDetailIndex = colliderLevelOfDetailIndex;

        this.heightMapSettings = heightMapSettings;
        this.meshSettings = meshSettings;

        this.viewer = viewer;

        // Get the actual in game x and y coords for the terrain chunk
        sampleCenter = coord * meshSettings.meshWorldSize / meshSettings.meshScale;
        Vector2 position = coord * meshSettings.meshWorldSize;
        bounds = new Bounds(position, Vector3.one * meshSettings.meshWorldSize);

        // Create a placeholder plane for each terrain chunk
        meshObject = new GameObject("Terrain Chunk");

        // Add the mesh renderer, mesh filter and mesh collider
        meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshRenderer.material = material;
        meshFilter = meshObject.AddComponent<MeshFilter>();
        meshCollider = meshObject.AddComponent<MeshCollider>();

        // Position the terrain chunk
        meshObject.transform.position = new Vector3(position.x, 0, position.y);
        meshObject.transform.parent = parent;

        // Initially set the new plane to be invisible. The TerrainChunk's UpdateTerrainChunk will handle whether or not it should be visible
        SetVisible(false);

        // Initialize the array of level of detail meshes for this terrain chunk
        levelOfDetailMeshes = new LevelOfDetailMesh[detailLevels.Length];
        for (int currLevelOfDetail = 0; currLevelOfDetail < detailLevels.Length; currLevelOfDetail++)
        {
            levelOfDetailMeshes[currLevelOfDetail] = new LevelOfDetailMesh(detailLevels[currLevelOfDetail].levelOfDetail);

            // Set the callback functions once the level of detail mesh is created. Once it is created, call UpdateTerrainChunk. 
            // If this mesh also happens to be the requested collider mesh, also set that it should update the collision mesh once generated.
            levelOfDetailMeshes[currLevelOfDetail].updateCallback += UpdateTerrainChunk;
            if (currLevelOfDetail == colliderLevelOfDetailIndex)
            {
                levelOfDetailMeshes[currLevelOfDetail].updateCallback += UpdateCollisionMesh;
            }
        }

        // Init the max view distance for this chunk
        maxViewDistance = detailLevels[detailLevels.Length - 1].visibleDistThreshold;

    }

    public void Load()
    {
        // Request the map data for this terrain chunk
        // We use a lambda expression here to make a temporary function which takes no parameters, which is needed for the RequestData method.
        ThreadedDataRequester.RequestData(() => HeightMapGenerator.GenerateHeightMap(meshSettings.numVertsPerLine, meshSettings.numVertsPerLine, heightMapSettings, sampleCenter), OnHeightMapRecieved);
    }

    Vector2 viewerPosition
    {
        get { return new Vector2(viewer.position.x, viewer.position.z); }
    }

    /// <summary>
    /// Callback function for when the endless terrain script recieved the map data back from the MapPreview script.
    /// </summary>
    /// <param name="heightMap">The generated map data from the map generator</param>
    void OnHeightMapRecieved(object heightMap)
    {
        // Cache off the requested map data and flag that we have it
        this.heightMap = (HeightMap)heightMap;
        heightMapReceived = true;

        // Forcefully update this terrain chunk so that we can start requesting the proper LOD and generate the proper mesh
        UpdateTerrainChunk();
    }

    /// <summary>
    /// Finds what level of detail should be used by this terrain chunk based on the specified viewer distance from the nearest terrain chunk edge
    /// </summary>
    /// <param name="viewerDistanceFromNearestEdge">The distance the viewer is from the closest edge of this terrain chunk</param>
    /// <returns></returns>
    private int GetLevelOfDetailForViewDistance(float viewerDistanceFromNearestEdge)
    {
        int levelOfDetailIndex = 0;

        // Loop through each authored level of detail index and check to see if the viewers current distance is valid for that detail level. We don't need to look at the last detail level (detailLevels.Length - 1) because in that case, the terrain chunk won't be visible
        for (int currDetailIndex = 0; currDetailIndex < detailLevels.Length - 1; currDetailIndex++)
        {
            // If the viewer is further away than the current detail level, the next level of detail should be used
            if (viewerDistanceFromNearestEdge > detailLevels[currDetailIndex].visibleDistThreshold)
            {
                levelOfDetailIndex = currDetailIndex + 1;
            }
            else
            {
                // Once we find a level of detail that is farther away than the viewer, use whatever the last valid level of detail we found
                break;
            }
        }

        return levelOfDetailIndex;
    }

    public void UpdateTerrainChunk()
    {
        // If we haven't yet recieved the map data, don't update the chunk
        if (!heightMapReceived)
        {
            return;
        }

        // Get the distance between the edge of this terrain chunk and the viewer
        float viewerDistanceFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(viewerPosition));

        // If the viewer is close enough, set the terrain chunk visible. Otherwise, set it invisible.
        bool wasVisible = IsVisible();
        bool visible = viewerDistanceFromNearestEdge <= maxViewDistance;

        // If the terrain chunk should be visible, determine the level of detail it should display at
        if (visible)
        {
            // Get the level of detail index for the current view distance
            int desiredLevelOfDetailIndex = GetLevelOfDetailForViewDistance(viewerDistanceFromNearestEdge);

            if (desiredLevelOfDetailIndex == -1)
            {
                Debug.Assert(desiredLevelOfDetailIndex != -1, "Was unable to find a valid level of detail index for distance " + viewerDistanceFromNearestEdge);
                desiredLevelOfDetailIndex = 0;
            }

            // If the currently desired level of detail index does not match the previous update's level of detail index, update the LevelOfDetailMesh now
            if (desiredLevelOfDetailIndex != previousLevelOfDetailIndex)
            {
                LevelOfDetailMesh levelOfDetailMesh = levelOfDetailMeshes[desiredLevelOfDetailIndex];

                // Check to see if the mesh for the desired level of detail has already been created. If not, create it.
                if (levelOfDetailMesh.hasMesh)
                {
                    meshFilter.mesh = levelOfDetailMesh.mesh;
                    previousLevelOfDetailIndex = desiredLevelOfDetailIndex;
                }
                else if (!levelOfDetailMesh.hasRequestedMesh)
                {
                    levelOfDetailMesh.RequestMesh(heightMap, meshSettings);
                }
            }
        }

        if (wasVisible != visible)
        {
            // Set the chunks visibility
            SetVisible(visible);

            // If the onVisibilityChanged action has been defined, call it now
            if (onVisibilityChanged != null)
            {
                onVisibilityChanged(this, visible);
            }
        }
    }

    public void UpdateCollisionMesh()
    {
        // Only need to update the collision mesh if we haven't done so already
        if (hasSetCollider)
        {
            return;
        }

        // Get the viewers square distance to the edge of this terrain chunk
        float sqrDistanceFromViewerToEdge = bounds.SqrDistance(viewerPosition);

        // As a safety to ensure that the mesh we want to use for this chunks collision actually exists, request the mesh if we are within the visible distance of it
        if (sqrDistanceFromViewerToEdge < detailLevels[colliderLevelOfDetailIndex].sqrVisibleDistanceThreshold)
        {
            if (!levelOfDetailMeshes[colliderLevelOfDetailIndex].hasRequestedMesh)
            {
                levelOfDetailMeshes[colliderLevelOfDetailIndex].RequestMesh(heightMap, meshSettings);
            }
        }

        // If the sqr distance is within the sqr distance defined to generate the chunks collider, set the collider mesh
        if (sqrDistanceFromViewerToEdge < COLLIDER_GENERATION_DISTANCE_THRESHOLD * COLLIDER_GENERATION_DISTANCE_THRESHOLD)
        {
            if (levelOfDetailMeshes[colliderLevelOfDetailIndex].hasMesh)
            {
                meshCollider.sharedMesh = levelOfDetailMeshes[colliderLevelOfDetailIndex].mesh;
                hasSetCollider = true;
            }
        }
    }

    public void SetVisible(bool visible)
    {
        meshObject.SetActive(visible);
    }

    public bool IsVisible()
    {
        return meshObject.activeSelf;
    }
}

class LevelOfDetailMesh
{
    public Mesh mesh;
    public bool hasRequestedMesh;
    public bool hasMesh;
    int LOD;
    public event System.Action updateCallback;

    public LevelOfDetailMesh(int LOD)
    {
        this.LOD = LOD;
    }

    public void OnMeshDataRecieved(object meshDataObject)
    {
        // Create the mesh with the supplied mesh data and flag that this level of detail has a mesh
        mesh = ((MeshData)meshDataObject).CreateMesh();
        hasMesh = true;

        // Forcefully update the terrain chunk so that it can set itself as visible
        updateCallback();
    }

    public void RequestMesh(HeightMap heightMap, MeshSettings meshSettings)
    {
        hasRequestedMesh = true;
        ThreadedDataRequester.RequestData(() => MeshGenerator.GenerateTerrainMesh(heightMap.values, meshSettings, LOD), OnMeshDataRecieved);
    }
}