﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    // The distance that the viewer needs to move before the chunks will update
    const float VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE = 25f;
    const float SQR_VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE = VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE * VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE; // Optimization. Can do cheaper distance checks without needing to do sqrt

    public int colliderLevelOfDetailIndex;
    public LevelOfDetailInfo[] detailLevels;

    public MeshSettings meshSettings;
    public HeightMapSettings heightMapSettings;
    public TextureSettings textureSettings;

    public Transform viewer;
    public Material mapMaterial;

    private Vector2 viewerPosition;
    private Vector2 oldViewerPosition;

    float meshWorldSize;
    int chunksVisibleInViewDist;

    Dictionary<Vector2, TerrainChunk> terrainChunkDict = new Dictionary<Vector2, TerrainChunk>();
    List<TerrainChunk> visibleTerrainChunks = new List<TerrainChunk>();

    public void Start()
    {
        // Set all the necessary matieral data
        textureSettings.ApplyToMaterial(mapMaterial);

        // Send the min and max heights of this terrain to the texture data struct so that it is able to be used in the terrain shader
        textureSettings.UpdateMeshHeights(mapMaterial, heightMapSettings.minHeight, heightMapSettings.maxHeight);

        // Initialize the max view dist
        float maxViewDist = detailLevels[detailLevels.Length - 1].visibleDistThreshold;

        // The numVertsPerLine refers to the number of vertices within a map chunk, while this value corresponds to the actual dimension of the chunk meshWorldSize.
        meshWorldSize = meshSettings.meshWorldSize;
        chunksVisibleInViewDist = Mathf.RoundToInt(maxViewDist / meshWorldSize);

        // Force the chunks to update for the first time
        UpdateVisibleChunks();
    }

    public void Update()
    {
        viewerPosition = new Vector2(viewer.position.x, viewer.position.z);

        // Each time the player moves, update the visible terrain chunk's collision to see if it should be enabled
        if(viewerPosition != oldViewerPosition)
        {
            foreach (TerrainChunk chunk in visibleTerrainChunks)
            {
                chunk.UpdateCollisionMesh();
            }
        }

        // If we've moved further than SQR_VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE from the last position that we updated the chunks, update the chunks again now
        if ((oldViewerPosition - viewerPosition).sqrMagnitude > SQR_VIEWER_MOVE_THRESHOLD_FOR_CHUNK_UPDATE)
        {
            oldViewerPosition = viewerPosition;
            UpdateVisibleChunks();
        }
    }

    private void UpdateVisibleChunks()
    {
        HashSet<Vector2> updatedChunkCoords = new HashSet<Vector2>();

        // Run each chunk's update. We run through the list in reverse in case the terrain chunk's update causes it to be removed from the list
        for (int currChunk = visibleTerrainChunks.Count - 1; currChunk > 0; currChunk--)
        {
            // Add the terrain chunk to the updated terrain chunks list
            updatedChunkCoords.Add(visibleTerrainChunks[currChunk].coord);

            // Update this terrain chunk
            visibleTerrainChunks[currChunk].UpdateTerrainChunk();
        }

        // Get the x and y coord of the chunk that the viewer is currently in
        int currentChunkCoordX = Mathf.RoundToInt(viewerPosition.x / meshWorldSize);
        int currentChunkCoordY = Mathf.RoundToInt(viewerPosition.y / meshWorldSize);

        // Loop through all chunks within the viewable distance
        for(int yOffset = -chunksVisibleInViewDist; yOffset <= chunksVisibleInViewDist; yOffset++)
        {
            for(int xOffset = -chunksVisibleInViewDist; xOffset <= chunksVisibleInViewDist; xOffset++)
            {
                // Grab the coord for the current chunk
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

                // If we've already updated this terrain chunk, we don't need to update it again
                if (!updatedChunkCoords.Contains(viewedChunkCoord))
                {

                    // If the coordinate already exists, update it
                    if (terrainChunkDict.ContainsKey(viewedChunkCoord))
                    {

                        // Grab the chunk so that we don't need to continue looking into the dictionary
                        TerrainChunk viewedChunk = terrainChunkDict[viewedChunkCoord];

                        // Update the terrain chunk so that it can potentially set itself as visible
                        viewedChunk.UpdateTerrainChunk();
                    }
                    else
                    {
                        // Create the new terrain chunk and add it to the dictionary
                        TerrainChunk newChunk = new TerrainChunk(viewedChunkCoord, heightMapSettings, meshSettings, detailLevels, colliderLevelOfDetailIndex, transform, viewer, mapMaterial);
                        terrainChunkDict.Add(viewedChunkCoord, newChunk);

                        // Set the onVisibiltyChanged action to call the OnTerrainChunkVisbilityChanged method
                        newChunk.onVisibilityChanged += OnTerrainChunkVisibilityChanged;

                        // Trigger the logic which generates the heightmap and mesh for this terrain chunk
                        newChunk.Load();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Adds or removes the specified chunk from the visible terrain chunks based on if the chunk is visible or not
    /// </summary>
    /// <param name="terrainChunk"></param>
    /// <param name="isVisible"></param>
    void OnTerrainChunkVisibilityChanged(TerrainChunk terrainChunk, bool isVisible)
    {
        if(isVisible)
        {
            visibleTerrainChunks.Add(terrainChunk);
        }
        else
        {
            visibleTerrainChunks.Remove(terrainChunk);
        }
    }
}

[System.Serializable]
public struct LevelOfDetailInfo
{
    [Range(0, MeshSettings.NUM_SUPPORTED_LEVELS_OF_DETAIL - 1)]
    public int levelOfDetail;
    public float visibleDistThreshold;

    public float sqrVisibleDistanceThreshold
    {
        get { return visibleDistThreshold * visibleDistThreshold; }
    }
}
