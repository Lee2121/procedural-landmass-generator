﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoiseMap
{
    // The maximum value of the random offset generated in GenerateRandomOffsetForOctaves
    const int MAX_RAND_OFFSET = 100000;

    /// <summary>
    /// Generates a height value (between -1 and 1) for the supplied x,y coordinate using perlin noise.
    /// </summary>
    /// <param name="xCoord"></param>
    /// <param name="yCoord"></param>
    /// <param name="noiseMapParams"></param>
    /// <param name="octaveOffsets">A list of offsets to use for each octave</param>
    /// <param name="centerCoord">The 2d coordinate of the center position of the noise map. Used to scale to/from the center rather than from the corner</param>
    /// <returns>A generated height value between -1 and 1</returns>
    private static float GenerateHeightForCoord(int xCoord, int yCoord, NoiseSettings noiseSettings, Vector2[] octaveOffsets, Vector2 centerCoord)
    {
        // Initialize data
        float amplitude = 1;
        float frequency = 1;
        float returnHeight = 0;
        
        // Perform a loop for each octave (level of detail) to determine the height for the given coord
        for (int iCurrOctave = 0; iCurrOctave < noiseSettings.octaves; iCurrOctave++)
        {
            float scaledXCoord = (xCoord - centerCoord.x) / noiseSettings.scale;
            float scaledYCoord = (yCoord - centerCoord.y) / noiseSettings.scale;

            // Get the coordinates to supply to the perlin noise function.
            float sampleWidth = (scaledXCoord + octaveOffsets[iCurrOctave].x) * frequency;
            float sampleHeight = (scaledYCoord + octaveOffsets[iCurrOctave].y) * frequency;

            // Get the value of the perlin noise at the given sample width and height location
            // The perlin noise function returns a value between 0 and 1, but because we want a value between -1 and 1, multiply the perlin noise result by 2 and then subtract 1
            float perlinValue = Mathf.PerlinNoise(sampleWidth, sampleHeight) * 2 - 1;
            returnHeight += perlinValue * amplitude;

            // Apply the persistance and lacunarity values to the modifiers for the next octave
            amplitude *= noiseSettings.persistance; // Amplitude - each octave, change how much of an effect the octave has on the total height map
            frequency *= noiseSettings.lacunarity; // Frequency - each octave, change the amount of fine detail
        }
        
        return returnHeight;
    }

    /// <summary>
    /// Generates a random 2d offset for each of the octaves
    /// </summary>
    /// <param name="seed">Used to seed the random number generator</param>
    /// <param name="numOctaves">The number of ocatves to generate offsets for</param>
    /// <param name="baseOffset">A base offset to apply to all generated offsets</param>
    /// <returns>A list of 2d offsets</returns>
    private static Vector2[] GenerateRandomOffsetForOctaves(int seed, int numOctaves, Vector2 baseOffset, Vector2 sampleCenter)
    {
        System.Random pseudoRandomNum = new System.Random(seed);

        Vector2[] randomOffsets = new Vector2[numOctaves];

        for (int currOctave = 0; currOctave < numOctaves; currOctave++)
        {
            float offsetX = pseudoRandomNum.Next(-MAX_RAND_OFFSET, MAX_RAND_OFFSET) + baseOffset.x + sampleCenter.x;
            float offsetY = pseudoRandomNum.Next(-MAX_RAND_OFFSET, MAX_RAND_OFFSET) - baseOffset.y - sampleCenter.y;

            randomOffsets[currOctave] = new Vector2(offsetX, offsetY);
        }

        return randomOffsets;
    }

    /// <summary>
    /// Normalizes all coordinates in the noiseMap 2d array to be a value between 0 and 1, based on the min and max height of the supplied noise map
    /// </summary>
    /// <param name="noiseMap"></param>
    /// <param name="maxHeight"></param>
    /// <param name="minHeight"></param>
    private static void NormalizeNoiseMapLocal(ref float[,] noiseMap, float minHeight, float maxHeight)
    {
        // Loop through each coordiante on the 2d grid
        for (int yCoord = 0; yCoord < noiseMap.GetLength(1); yCoord++)
        {
            for (int xCoord = 0; xCoord < noiseMap.GetLength(0); xCoord++)
            {
                // Set the value at this coord to be between 0 and 1
                noiseMap[xCoord, yCoord] = Mathf.InverseLerp(minHeight, maxHeight, noiseMap[xCoord, yCoord]);
            }
        }
    }

    /// <summary>
    /// Estimates what the highest possible noise height will be for the given noise map params.
    /// Does this by looping through all of the octaves in the noise map, and adding the amplitude of each octave.
    /// </summary>
    /// <param name="noiseSettings">The noise map params to estimate the max possible height for</param>
    /// <returns>The estimated highest possible height for the supplied noiseMapParams</returns>
    private static float EstimateHighestPossibleNoiseHeight(NoiseSettings noiseSettings)
    {
        // Initialize values
        float maxPossibleHeight = 0;
        float amplitude = 1;

        // Loop through each octave and add its amplitude to the maxPossibleHeightValue.
        for (int currOctave = 0; currOctave < noiseSettings.octaves; currOctave++)
        {
            maxPossibleHeight += amplitude;
            amplitude *= noiseSettings.persistance;
        }

        return maxPossibleHeight;
    }

    /// <summary>
    /// Normalizes the noiseMap array based on the max possible noise height
    /// </summary>
    /// <param name="noiseMap"></param>
    /// <param name="noiseSettings"></param>
    private static void NormalizeNoiseMapGlobal(ref float[,] noiseMap, NoiseSettings noiseSettings)
    {
        // Calculate the highest possible point on the noise map
        float maxGlobalNoiseHeight = EstimateHighestPossibleNoiseHeight(noiseSettings);

        // Loop through each coordiante on the 2d grid
        for (int yCoord = 0; yCoord < noiseMap.GetLength(1); yCoord++)
        {
            for (int xCoord = 0; xCoord < noiseMap.GetLength(0); xCoord++)
            {
                // In GenerateHeightForCoord we calculate the perlinValue within a range of -1 and 1 by multiplying the perlin noise output by 2 and subtracting 1, 
                // we need to do the inverse here (add 1 to the cached noiseMap value and then divide by 2)
                float normalizedHeight = (noiseMap[xCoord, yCoord] + 1) / (2f * maxGlobalNoiseHeight / 2f); // This second 2 is used to further modify the max height value. Increasing it allows the max height to be higher, while decreasing it forces it to be lower.

                // Clamp the normalized height to be at least greater than 0
                normalizedHeight = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);

                // Assign the normalized height to the noise map
                noiseMap[xCoord, yCoord] = normalizedHeight;
            }
        }
    }

    /// <summary>
    /// Handles generating a noise map built with the supplied NoiseMapParams.
    /// </summary>
    /// <param name="noiseMapParams">Filled out struct containing the parameters for the generated noise map</param>
    /// <param name="width">Width of the desired generated noise map</param>
    /// <param name="height">Height of the desired generated noise map</param>
    /// <returns></returns>
    public static float[,] GenerateNoiseMap(int width, int height, NoiseSettings noiseSettings, Vector2 sampleCenter)
    {
        float[,] noiseMap = new float[width, height];
        
        // Get a list of random offsets for each octave
        Vector2[] octaveOffsets = GenerateRandomOffsetForOctaves(noiseSettings.seed, noiseSettings.octaves, noiseSettings.offset, sampleCenter);

        // Get the center coordiante of the noise map
        Vector2 centerCoord = new Vector2(width / 2, height / 2);

        // Used to normalize the noiseMap once we have generated all heights
        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;
        
        // Loop through each coordiante on the 2d grid
        for(int yCoord = 0; yCoord < height; yCoord++)
        {
            for(int xCoord = 0; xCoord < width; xCoord++)
            {
                // Generate the height at the given coordinate using the given parameters
                float coordHeight = GenerateHeightForCoord(xCoord, yCoord, noiseSettings, octaveOffsets, centerCoord);

                // Track what the highest and lowest heights are
                if(coordHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = coordHeight;
                }

                if (coordHeight < minLocalNoiseHeight)
                {
                    minLocalNoiseHeight = coordHeight;
                }

                // Cache off the calculated height for the current coord
                noiseMap[xCoord, yCoord] = coordHeight;
            }
        }

        // Normalize all values within the noiseMap to be within -1 and 1
        // If the normalize mode is local, normalize within the min and max height found within this individual noise map
        // If the normalize mode is global, normalize against the highest and lowest possible values
        switch(noiseSettings.normalizeMode)
        {
            case NoiseSettings.NormalizeMode.local:
                NormalizeNoiseMapLocal(ref noiseMap, minLocalNoiseHeight, maxLocalNoiseHeight);
                break;

            case NoiseSettings.NormalizeMode.global:
                NormalizeNoiseMapGlobal(ref noiseMap, noiseSettings);
                break;
        }
        
        // Return the generated noiseMap
        return noiseMap;
    }
}

[System.Serializable]
public class NoiseSettings
{
    // Random number generator seed
    public int seed;

    // Used to offset the generated noise map if desired
    public Vector2 offset;

    // Scales the generated noise map
    public float scale = 50;

    // Levels of detail the noise will have
    // More octaves results in more noise
    // Eg. Octave 1 could be mountains, octave 2 could be boulders, octave 3 could be rocks
    public int octaves = 6;

    // How much each octave contributes tothe overall shape of the noise
    // This adjusts the amplitude of the noise
    [Range(0, 2)]
    public float persistance = .6f;

    // How much detail is added or removed at each octave 
    // Adjusts the frequency of the noise
    // More than 1 means each octave will increase its level of detail
    // 1 means each octave will have the same level of detail
    // Less than 1 means each octave will get smoother
    // Suggested default of 2
    public float lacunarity = 2;

    // Method that the noise map is normalized in PerlinNoiseMap.NormalizeNoiseMap.
    public enum NormalizeMode
    {
        local,  // Used when only generating a single terrain tile. Normalization is applied based on the highest and lowest heights of the individual chunks height map
        global  // Used when generating many tiles at once that are meant to seamlessly transition from one to another, such as through the endless terrain script. Uses estimated high and low values to get a consistent normalization across many unique terrain tiles at the cost of having the normalization clamp values
    };
    public NormalizeMode normalizeMode;

    public void ValidateValues()
    {
        scale = Mathf.Max(scale, 0.01f);
        octaves = Mathf.Max(octaves, 1);
        lacunarity = Mathf.Max(lacunarity, 1);
        persistance = Mathf.Clamp01(persistance);
    }
}