﻿Shader "Custom/Terrain"
{
	Properties
	{
		testTexture("Texture", 2D) = "white"{}
		testScale("Scale", Float) = 1
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM

		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		const static int maxLayerCount = 8;
		const static float epsilon = -1E-4;

		int layerCount;
		float3 baseColors[maxLayerCount];
		float baseStartHeights[maxLayerCount];
		float baseBlends[maxLayerCount];
		float baseColorStrengths[maxLayerCount];
		float baseTextureScales[maxLayerCount];

		float minHeight;
		float maxHeight;

		sampler2D testTexture;
		float testScale;

		UNITY_DECLARE_TEX2DARRAY(baseTextures);

        struct Input
        {
			float3 worldPos;
			float3 worldNormal;
        };

		float inverseLerp(float a, float b, float value)
		{
			// If a is 0, set it to the epsilon value so that we don't end up dividing by 0
			if (a == 0)
			{
				a = epsilon;
			}

			// saturate clamps the output between 0 and 1
			return saturate((value - a) / (b - a));
		}

		// To avoid stretching of the texture when applied to surfaces at any angle, 
		// we project the texture along all three planes and weigh the result according to the blendAxis value.
		// In this case, the blendAxis is the normal at that world position
		float3 triplaner(float3 worldPos, float scale, float3 blendAxis, int textureIndex)
		{
			float3 scaledWorldPos = worldPos / scale;

			float3 xProjection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaledWorldPos.y, scaledWorldPos.z, textureIndex)) * blendAxis.x;
			float3 yProjection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaledWorldPos.x, scaledWorldPos.z, textureIndex)) * blendAxis.y;
			float3 zProjection = UNITY_SAMPLE_TEX2DARRAY(baseTextures, float3(scaledWorldPos.x, scaledWorldPos.y, textureIndex)) * blendAxis.z;
			
			return xProjection + yProjection + zProjection;
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float heightPercent = inverseLerp(minHeight, maxHeight, IN.worldPos.y);
			
			// Determines how much each projection will affect the output based on the normal of the pixel
			// blendAxis is divided by the sum of its parts to ensure that all values remain between 0 and 1
			float3 blendAxis = abs(IN.worldNormal);
			blendAxis /= blendAxis.x + blendAxis.y + blendAxis.z;

			for (int currLayer = 0; currLayer < layerCount; currLayer++)
			{
				// Calculate the blend value at our current height
				float baseBlendValue = baseBlends[currLayer] / 2;

				float drawStrength = inverseLerp(baseBlendValue * -1, baseBlendValue, heightPercent - baseStartHeights[currLayer]);

				float3 baseColor = baseColors[currLayer] * baseColorStrengths[currLayer];
				float3 textureColor = triplaner(IN.worldPos, baseTextureScales[currLayer], blendAxis, currLayer) * (1 - baseColorStrengths[currLayer]);

				// Fade the colors into each other. The draw strength dictates how strongly the current height color is applied to the overall output.
				// If the drawStrength ends up be 0, the below logic will just set the albedo equal to itself.
				// Otherwise, the current height color is added to the albedo.
				o.Albedo = o.Albedo * (1 - drawStrength) + (baseColor + textureColor) * drawStrength;
			}

		}
        ENDCG
    }
    FallBack "Diffuse"
}
